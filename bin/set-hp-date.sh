#!/bin/sh

########################################
#### Termin auf HP setzten
########################################

prefix="Die nächste *#devlug* ist am"
postfix="um 20:30 Uhr"

midfix=$(stammtisch --irc)
midfix=${midfix%noch*}
midfix=${midfix#*ist am }
midfix=$(date -d $midfix '+%A, den %d. %B %Y')

mkdir -p /home/devlug/git/devlug-web/www/jbake/content/import
echo "$prefix $midfix $postfix" > /home/devlug/git/devlug-web/www/jbake/content/import/info.homepage.adoc
/home/devlug/bin/gitdeploy-v2.sh master

exit $?
