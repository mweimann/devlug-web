= Debian GNU/Linux 
DebXWoody
2019-04-07
:jbake-type: post
:jbake-status: published
:jbake-tags: linux,debian,qemu
:jbake-updated: So 29. Sep 19:28:18 CEST 2019
:jbake-regal: Linux
:idprefix:
:toc:
[abstract]
Informationen über Debian GNU/Linux

== Allgemeine Informationen 
Allgemeine Informationen auf link:https://de.wikipedia.org/wiki/Debian[Wikipedia].
Offizielle Homepage link:https://www.debian.org/[debian.org].

----
⢀⣴⠾⠻⢶⣦⠀
⣾⠁⢰⠒⠀⣿⡁
⢿⡄⠘⠷⠚⠋⠀
⠈⠳⣄⠀⠀⠀⠀
----

== Dokumentation

* link:https://www.debian.org/doc/manuals/debian-reference/[Debian-Referenz]
* link:https://www.debian.org/distrib/packages[Paketsuche]
* link:https://wiki.debian.org[Debian Wiki]

== Installation

=== Vorbereitung
Die Debian CDs werden von Debian unterschrieben. Informationen findet man auf
der Homepage von Debian: https://www.debian.org/CD/verify

Der Schlüssel ist auch im Debian Paket debian-keyring.

	apt-get install debian-keyring

Debian ISO Image von der Homepage runterladen.
https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/

----
wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.1.0-amd64-netinst.iso
wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/SHA512SUMS
wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/SHA512SUMS.sign
----

----
% gpg --keyring /usr/share/keyrings/debian-role-keys.gpg --verify SHA512SUMS.sign SHA512SUMS 
gpg: Signatur vom So 08 Sep 2019 17:52:40 CEST
gpg:                mittels RSA-Schlüssel DF9B9C49EAA9298432589D76DA87E80D6294BE9B
gpg: Korrekte Signatur von "Debian CD signing key <debian-cd@lists.debian.org>" [unbekannt]
gpg: WARNUNG: Dieser Schlüssel trägt keine vertrauenswürdige Signatur!
gpg:          Es gibt keinen Hinweis, daß die Signatur wirklich dem vorgeblichen Besitzer gehört.
Haupt-Fingerabdruck  = DF9B 9C49 EAA9 2984 3258  9D76 DA87 E80D 6294 BE9B
----

Mit dem Befehl kann sichergestellt werden, dass der Inhalt der Datei
`SHA512SUMS` mit dem oben genanten Schlüssel unterschrieben wurde. Die
`SHA512SUMS.sign` beinhaltet die Signatur der Datei.

Man kann nun die ISO Datei mit den Informationen in SHA512SUMS prüfen.

----
% sha512sum -c SHA512SUMS
debian-10.1.0-amd64-netinst.iso: OK
----

Wir installieren das System für unser Beispiel in einer virtuellen Maschine.

----
qemu-img create  -f qcow2 devlug-debian.img 15G
qemu-system-x86_64 -enable-kvm -m 4G -cdrom ~/Downloads/debian-10.1.0-amd64-netinst.iso -hda devlug-debian.img
----

In diesem Beispiel verwenden wir eine 15GB Festplatte und das System wird mit 4
GB RAM ausgestattet. Diese Werte könnte nach belieben angepasst werden, sofern
das Hostsystem über genügend Ressourcen verfügt. 


image:/img/blog/linux/Debain-Install-01.png[]
image:/img/blog/linux/Debain-Install-02.png[]
image:/img/blog/linux/Debain-Install-03.png[]
image:/img/blog/linux/Debain-Install-04.png[]
image:/img/blog/linux/Debain-Install-05.png[]
image:/img/blog/linux/Debain-Install-06.png[]
image:/img/blog/linux/Debain-Install-07.png[]
image:/img/blog/linux/Debain-Install-08.png[]
image:/img/blog/linux/Debain-Install-09.png[]
image:/img/blog/linux/Debain-Install-10.png[]
image:/img/blog/linux/Debain-Install-11.png[]
image:/img/blog/linux/Debain-Install-12.png[]
image:/img/blog/linux/Debain-Install-13.png[]
image:/img/blog/linux/Debain-Install-14.png[]
image:/img/blog/linux/Debain-Install-15.png[]
image:/img/blog/linux/Debain-Install-16.png[]
image:/img/blog/linux/Debain-Install-17.png[]
image:/img/blog/linux/Debain-Install-18.png[]
image:/img/blog/linux/Debain-Install-19.png[]
image:/img/blog/linux/Debain-Install-20.png[]
image:/img/blog/linux/Debain-Install-21.png[]
image:/img/blog/linux/Debain-Install-22.png[]
image:/img/blog/linux/Debain-Install-23.png[]
image:/img/blog/linux/Debain-Install-24.png[]
image:/img/blog/linux/Debain-Install-25.png[]
image:/img/blog/linux/Debain-Install-26.png[]
image:/img/blog/linux/Debain-Install-27.png[]
image:/img/blog/linux/Debain-Install-28.png[]
image:/img/blog/linux/Debain-Install-29.png[]
image:/img/blog/linux/Debain-Install-30.png[]
image:/img/blog/linux/Debain-Install-31.png[]




